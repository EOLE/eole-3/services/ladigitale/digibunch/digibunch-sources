<?php

session_start();

require 'headers.php';

if (!empty($_POST['bouquet']) && !empty($_POST['nouveaunom'])) {
	require 'db.php';
	$reponse = '';
	$bouquet = $_POST['bouquet'];
	if (isset($_SESSION['digibunch'][$bouquet]['reponse'])) {
		$reponse = $_SESSION['digibunch'][$bouquet]['reponse'];
	}
	$stmt = $db->prepare('SELECT reponse FROM digibunch_bouquets WHERE url = :url');
	if ($stmt->execute(array('url' => $bouquet))) {
		$resultat = $stmt->fetchAll();
		if ($resultat[0]['reponse'] === $reponse) {
			$nouveaunom = $_POST['nouveaunom'];
			$stmt = $db->prepare('UPDATE digibunch_bouquets SET nom = :nouveaunom WHERE url = :url');
			if ($stmt->execute(array('nouveaunom' => $nouveaunom, 'url' => $bouquet))) {
				echo 'nom_modifie';
			} else {
				echo 'erreur';
			}
		} else {
			echo 'non_autorise';
		}
	} else {
		echo 'erreur';
	}
	$db = null;
	exit();
} else {
	header('Location: ../');
	exit();
}

?>
