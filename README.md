# Digibunch

Digibunch est une application simple pour créer des bouquets de liens. 

Elle est publiée sous licence GNU AGPLv3.
Sauf les fontes Roboto Slab et Material Icons (Apache License Version 2.0) et la fonte HKGrotesk (Sil Open Font Licence 1.1)

### Préparation et installation des dépendances
```
npm install
```

### Lancement du serveur de développement
```
npm run dev
```

### Variable d'environnement (fichier .env.production à créer à la racine avant compilation)
Liste des domaines autorisés pour les requêtes POST et l'API, séparés par une virgule (* par défaut)
```
AUTHORIZED_DOMAINS=*
```

### Compilation et minification des fichiers
```
npm run build
```

### Serveur PHP nécessaire pour l'API
```
php -S 127.0.0.1:8000 (pour le développement uniquement)
```

### Production
Le dossier dist peut être déployé directement sur un serveur PHP avec l'extension SQLite activée.

### Démo
https://ladigitale.dev/digibunch/

### Remerciements et crédits
Traduction en italien par Paolo Mauri (https://codeberg.org/maupao)

### Soutien
Open Collective : https://opencollective.com/ladigitale

Liberapay : https://liberapay.com/ladigitale/
