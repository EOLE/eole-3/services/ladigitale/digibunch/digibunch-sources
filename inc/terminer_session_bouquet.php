<?php

session_start();

require 'headers.php';

if (!empty($_POST['bouquet'])) {
	$bouquet = $_POST['bouquet'];
	unset($_SESSION['digibunch'][$bouquet]['reponse']);
	echo 'session_terminee';
	exit();
} else {
	header('Location: ../');
	exit();
}

?>
