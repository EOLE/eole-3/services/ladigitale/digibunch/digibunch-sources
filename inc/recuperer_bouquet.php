<?php

session_start();

require 'headers.php';

if (!empty($_POST['id'])) {
	require 'db.php';
	$reponse = '';
	$id = $_POST['id'];
	if (isset($_SESSION['digibunch'][$id]['reponse'])) {
		$reponse = $_SESSION['digibunch'][$id]['reponse'];
	}
	$stmt = $db->prepare('SELECT * FROM digibunch_bouquets WHERE url = :url');
	if ($stmt->execute(array('url' => $id))) {
		if ($bouquet = $stmt->fetchAll()) {
			$admin = false;
			if (count($bouquet, COUNT_NORMAL) > 0 && $bouquet[0]['reponse'] === $reponse) {
				$admin = true;
			}
			$donnees = $bouquet[0]['donnees'];
			if ($donnees !== '') {
				$donnees = json_decode($donnees);
			}
			$digidrive = 0;
			if (isset($_SESSION['digibunch'][$id]['digidrive'])) {
				$digidrive = $_SESSION['digibunch'][$id]['digidrive'];
			}
			echo json_encode(array('nom' => $bouquet[0]['nom'], 'donnees' => $donnees, 'admin' =>  $admin, 'digidrive' => $digidrive));
		} else {
			echo 'contenu_inexistant';
		}
	} else {
		echo 'erreur';
	}
	$db = null;
	exit();
} else {
	header('Location: ../');
	exit();
}

?>
